
#pragma once
//#include "cutout_figure.h"
#include "figure.h"
#include <cmath>
#define _USE_MATH_DEFINES
class Circle: public virtual Figure {//{{{
protected:
	double radius;
public: 
	virtual double area();
	Circle(double );
}; //}}}
