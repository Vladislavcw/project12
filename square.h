
#pragma once
//#include "cutout_figure.h"
#include "figure.h"

class Square : public virtual Figure { //{{{
protected:
	double a;
public: 
	virtual double area ();
	Square(double );
}; //}}}
