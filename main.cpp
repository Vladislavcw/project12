#include <iostream>
#include "figure.h"
#include "circle.h"
#include "square.h"
#include "cutout_figure.h"
using namespace std;
int main(){
	Circle *circle1 = new Circle (5);
	Square *square1 = new Square (5);
	CircleCutSquare *cs1 = new CircleCutSquare (4, 2);
	SquareCutCircle *sc1 = new SquareCutCircle (4, 2);
	cout << circle1->area() << ' ' << square1->area() << ' ' << cs1->area() << ' ' << sc1->area() << endl; //78 25
	delete circle1;
	delete square1;
	return 0;
}
