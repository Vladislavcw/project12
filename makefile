

CFLAGS = -g -Wall
cutout_figure.o: cutout_figure.cpp cutout_figure.h
	g++ -c cutout_figure.cpp # -o cutout_figure.o
circle.o: circle.cpp circle.h
	g++ -c circle.cpp # -o circle.o
square.o: square.cpp square.h
	g++ -c square.cpp # -o square.o
#figure.o: figure.cpp figure.h
#	g++ -c figure.cpp # -o figure.o
main.o: main.cpp figure.h
	g++ -c main.cpp # -o main.o
all: main.o square.o circle.o cutout_figure.o 
	g++ main.o square.o circle.o cutout_figure.o -o prog
#all: main.cpp square.cpp circle.cpp cutout_figure.cpp figure.h
#	g++ main.cpp square.cpp circle.cpp cutout_figure.cpp -o prog
