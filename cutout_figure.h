
#pragma once
//#include "figure.h"
#include "circle.h"
#include "square.h"
//class Figure; class Circle; class Square;

class SquareCutCircle: public Square, public Circle{
public:
	double area();
	SquareCutCircle(double a, double radius);
};

class CircleCutSquare: public Square, public Circle{
public:
	double area();
	CircleCutSquare(double radius, double a);
};

SquareCutCircle::SquareCutCircle(double a, double radius):Circle(radius), Square(a){}

double SquareCutCircle::area(){return Square(this->a).area() - Circle(this->radius).area();}

CircleCutSquare::CircleCutSquare(double radius, double a):Circle(radius), Square(a){}

double CircleCutSquare::area(){return Circle(this->radius).area() - Square(this->a).area();}
